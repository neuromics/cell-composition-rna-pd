## htsjdk.samtools.metrics.StringHeader
# picard.analysis.CollectRnaSeqMetrics REF_FLAT=/ref/genomes/hg19/refFlat.txt.gz RIBOSOMAL_INTERVALS=/ref/genomes/hg19/hg19.rRNA.interval_list STRAND_SPECIFICITY=NONE METRIC_ACCUMULATION_LEVEL=[ALL_READS, READ_GROUP] INPUT=/data/dumitriu_rna_data/results/bamfiles/SRR2015731.bam OUTPUT=/data/dumitriu_rna_data/results/QC_postAln/SRR2015731_picardRNAmetrics.txt    MINIMUM_LENGTH=500 RRNA_FRAGMENT_PERCENTAGE=0.8 ASSUME_SORTED=true STOP_AFTER=0 VERBOSITY=INFO QUIET=false VALIDATION_STRINGENCY=STRICT COMPRESSION_LEVEL=5 MAX_RECORDS_IN_RAM=500000 CREATE_INDEX=false CREATE_MD5_FILE=false GA4GH_CLIENT_SECRETS=client_secrets.json
## htsjdk.samtools.metrics.StringHeader
# Started on: Tue May 29 09:49:31 CEST 2018

## METRICS CLASS	picard.analysis.RnaSeqMetrics
PF_BASES	PF_ALIGNED_BASES	RIBOSOMAL_BASES	CODING_BASES	UTR_BASES	INTRONIC_BASES	INTERGENIC_BASES	IGNORED_READS	CORRECT_STRAND_READS	INCORRECT_STRAND_READS	PCT_RIBOSOMAL_BASES	PCT_CODING_BASES	PCT_UTR_BASES	PCT_INTRONIC_BASES	PCT_INTERGENIC_BASES	PCT_MRNA_BASES	PCT_USABLE_BASES	PCT_CORRECT_STRAND_READS	MEDIAN_CV_COVERAGE	MEDIAN_5PRIME_BIAS	MEDIAN_3PRIME_BIAS	MEDIAN_5PRIME_TO_3PRIME_BIAS	SAMPLE	LIBRARY	READ_GROUP
6214546338	6070882474	8714	1933823089	2613217969	539718808	984113980	0	0	0	0.000001	0.318541	0.430451	0.088903	0.162104	0.748992	0.731677	0	0.568637	0.056839	0.38968	0.176757			
6214546338	6070882474	8714	1933823089	2613217969	539718808	984113980	0	0	0	0.000001	0.318541	0.430451	0.088903	0.162104	0.748992	0.731677	0	0.568637	0.056839	0.38968	0.176757	SRR2015731	LIB-SRR2015731	DUMITRIU.s1.31

## HISTOGRAM	java.lang.Integer
normalized_position	All_Reads.normalized_coverage	DUMITRIU.s1.31.normalized_coverage
0	0.020981	0.020981
1	0.035646	0.035646
2	0.074468	0.074468
3	0.136865	0.136865
4	0.200042	0.200042
5	0.268805	0.268805
6	0.328634	0.328634
7	0.384912	0.384912
8	0.443674	0.443674
9	0.495096	0.495096
10	0.538659	0.538659
11	0.58306	0.58306
12	0.623799	0.623799
13	0.658629	0.658629
14	0.686289	0.686289
15	0.713581	0.713581
16	0.740735	0.740735
17	0.767914	0.767914
18	0.790364	0.790364
19	0.809973	0.809973
20	0.825118	0.825118
21	0.838353	0.838353
22	0.855043	0.855043
23	0.872664	0.872664
24	0.889611	0.889611
25	0.905909	0.905909
26	0.922241	0.922241
27	0.931613	0.931613
28	0.944206	0.944206
29	0.959687	0.959687
30	0.97331	0.97331
31	0.984763	0.984763
32	0.996344	0.996344
33	1.003521	1.003521
34	1.01185	1.01185
35	1.023812	1.023812
36	1.029672	1.029672
37	1.036313	1.036313
38	1.045649	1.045649
39	1.056804	1.056804
40	1.062327	1.062327
41	1.06666	1.06666
42	1.072679	1.072679
43	1.082968	1.082968
44	1.095235	1.095235
45	1.106185	1.106185
46	1.110729	1.110729
47	1.126107	1.126107
48	1.137472	1.137472
49	1.144033	1.144033
50	1.151057	1.151057
51	1.157348	1.157348
52	1.16317	1.16317
53	1.17841	1.17841
54	1.191234	1.191234
55	1.20432	1.20432
56	1.217026	1.217026
57	1.220568	1.220568
58	1.223351	1.223351
59	1.227406	1.227406
60	1.237076	1.237076
61	1.245958	1.245958
62	1.251018	1.251018
63	1.25546	1.25546
64	1.268297	1.268297
65	1.281148	1.281148
66	1.291604	1.291604
67	1.305192	1.305192
68	1.318998	1.318998
69	1.326254	1.326254
70	1.330373	1.330373
71	1.335555	1.335555
72	1.335005	1.335005
73	1.342092	1.342092
74	1.343287	1.343287
75	1.350012	1.350012
76	1.358521	1.358521
77	1.361052	1.361052
78	1.359311	1.359311
79	1.365138	1.365138
80	1.376577	1.376577
81	1.393129	1.393129
82	1.389291	1.389291
83	1.375577	1.375577
84	1.359139	1.359139
85	1.349099	1.349099
86	1.330068	1.330068
87	1.322723	1.322723
88	1.329726	1.329726
89	1.310392	1.310392
90	1.278855	1.278855
91	1.25431	1.25431
92	1.206908	1.206908
93	1.137343	1.137343
94	1.039344	1.039344
95	0.924427	0.924427
96	0.799983	0.799983
97	0.635386	0.635386
98	0.420763	0.420763
99	0.200004	0.200004
100	0.066663	0.066663

