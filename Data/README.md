## Data folder


### Overview

* EnsDb.Hsapiens.v75.Rds
  Gene-transcript mapping from ensembl (v75). R object.

* sample_list_3cohorts_final.csv
  Metadata with sample information.

* countMatrix.genes
  RNA-seq count data.

* QC_postAln
  Folder with QC results from the alignments.

* Coverage.Rds
  Transcript coverage data.

* HumanNeuronVSglia.tsv
  List of neuronal markers (as opposed to markers specific only to some types of neurons).

* README.md
  This file.

* velmeshev_2019_PFC_snRNA/markers.csv
  File with snRNA data from PFC from the paper **Single-cell genomics identifies cell type–specific molecular changes in autism**, Science 17 May 2019: Vol. 364, Issue 6441, pp.685-689 DOI: 10.1126/science.aav8130

* kelley_2018_brain_sc/top100_markers.csv
  File with sc data from FC from the paper **Variation among intact tissue samples reveals the core transcriptional features of human CNS cell classes**, Nature Neuroscience volume 21, pages 1171–1184(2018).I have gathered the top 100 markers genes for each cell type using their kwebsite.

