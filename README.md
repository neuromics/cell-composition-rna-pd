Supplementary code for "Common signatures in Parkinson's disease are driven by changes in cell composition"
===========================================================================================================

Link to the paper in bioRxiv: https://doi.org/10.1101/778910

